import { useRouter } from "next/router";
import { ReactNode } from "react";

interface Iprops {
    children: ReactNode | ReactNode[];
    className?: string;
    href: string;
}

export default function NavButton(props: Iprops) {
    const router = useRouter();

    return (
        <button 
            className={props.className} 
            onClick={() => router.push(props.href)}
        >
            {props.children}
        </button>
    );
}

NavButton.defaultProps = {
    className: null,
    href: '#'
}