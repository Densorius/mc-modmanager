import SelectList from "../components/SelectList"

export default function Test() {

    const items = ["Cheese", "Tomato", "Pear", "Apple"]

    return (
        <div style={{
            margin: '8px',
            padding: '8px',
            borderRadius: '2px',
            boxShadow: '0px 0px 4px hsl(100, 0%, 70%)',
            width: '200px'
        }}>
            <SelectList items={items} />
        </div>
    )
}