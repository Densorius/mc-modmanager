import { app, BrowserWindow, ipcMain } from 'electron';
import next from '../../client/node_modules/next';
import path from 'path';
import http, { createServer } from 'http';
import { parse } from 'url';
import { AddressInfo } from 'net';

import installExtension, { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';

// Import the next config
// @ts-ignore implicit any
import nextConfig from '../../client/next.config.js';


export default function setup() {

    if (process.argv.length > 2 && process.argv[2] === '-d') {
        createWindowLocalhost();
    } else {
        // Await startup processes
        Promise.all([
            launchClient(),
            app.whenReady()
        ]).then(([ nextUrl ]) => {
        
            // Open the window now both Next and Electron
            // are done loading.
            createWindow(nextUrl);
        
            // pepega apple
            app.on('activate', () => {
                if (BrowserWindow.getAllWindows().length == 0) {
                    createWindow(nextUrl);
                }
            });

            // // install react extension
            // installExtension(REACT_DEVELOPER_TOOLS).then(name => {
            //     console.log(`Added Extension:  ${name}`);
            // }).catch(error => {
            //     console.log('An error occurred: ', error);
            // });
        });
    }
}

function createWindowLocalhost() {
    app.whenReady().then(() => {
        let url = 'http://localhost:3000/';

        createWindow(url);

        app.on('activate', () => {
            if (BrowserWindow.getAllWindows().length == 0) {
                createWindow(url);
            }
        });
    });
}

// Configure the next instance
function launchClient(): Promise<string> {
	return new Promise(async (resolve) => {
        console.log('resolving clientpath');
		const clientPath = path.resolve(__dirname, '../../client/');

        const nextApp = next({
            dev: true,
            ...nextConfig,
            _start: true,

            dir: clientPath,
            buildDir: clientPath + '/.next',
            rootDir: clientPath,
            srcDir: clientPath,
        });

        const requestHandler = nextApp.getRequestHandler();

        await nextApp.prepare();

        const server = createServer(requestHandler);

        server.listen(() => {
            const httpPort = (server.address() as AddressInfo).port;
            const address = `http://localhost:${httpPort}/`;

            resolve(address);

            app.on('before-quit', () => server.close());
        });
        
        // console.log('creating handle...');
        // const handle = nextApp.getRequestHandler();

        // console.log('preparing nextApp...');
        // nextApp.prepare().then(async () => {
        //     // Listen the Next server
            
        // });
	});
}

// Open the client window
function createWindow(url: string) {
    const window = new BrowserWindow({
		show: false,
        width: 1024,
        height: 768,
        minWidth: 640,
        minHeight: 480,
        icon: path.join(__dirname, '/images/mod_manager_v1.1.ico'),
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
        }
    });

    console.log('starting window...');
	window.loadURL(url).then(() => {
        window.show();
    }).catch(_ => {
        console.log('Failed to connect to localhost.\nMake sure you have started the server.');

        app.quit();
    });
}