import setup from "./setup";

import { app, ipcMain } from 'electron';
import fs from 'fs';
import os from 'os';
import path from 'path';

function getMods() {
	let modsPath: string = path.join(os.homedir(), '/AppData/Roaming/.minecraft/mods');

	fs.readdir(modsPath, (_, data) => {

		ipcMain.on('get-mods-async', event => {

			console.log('getting mods...');
			console.log(data);

			event.reply('mods-recieved', data);
		});
	});
}

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

setTimeout(() => {
	app.on('ready', () => {
		getMods();
	});
}, 0);

setup();