import {
    ipcRenderer,
    contextBridge
} from 'electron';

let mods: string[] = [];

ipcRenderer.send('get-mods-async');

ipcRenderer.on('mods-recieved', (_, arg) => {
    mods = arg;

    contextBridge.exposeInMainWorld('api', {
        get mods(): string[] {
            return mods;
        }
    });
});