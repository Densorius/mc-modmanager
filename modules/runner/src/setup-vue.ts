import { app, BrowserWindow, ipcMain } from 'electron';
import { Nuxt } from 'nuxt';
import path from 'path';
import http from 'http';
import { AddressInfo } from 'net';

// Import the nuxt config
// @ts-ignore implicit any
import nuxtConfig from '../../client/nuxt.config.js';

export default function setup() {
    // Await startup processes
    Promise.all([
        launchClient(),
        app.whenReady()
    ]).then(([ nuxtUrl ]) => {
    
        // Open the window now both Nuxt and Electron
        // are done loading.
        createWindow(nuxtUrl);
    
        // pepega apple
        app.on('activate', () => {
            if (BrowserWindow.getAllWindows().length == 0) {
                createWindow(nuxtUrl);
            }
        });
    });
}

// Configure the nuxt instance
function launchClient(): Promise<string> {
	return new Promise(async (resolve) => {
		const clientPath = path.resolve(__dirname, '../../client/');
		const nuxt = new Nuxt({
			...nuxtConfig,
			_start: true,
			dev: false,
			buildDir: clientPath + '/.nuxt',
			rootDir: clientPath,
			srcDir: clientPath,
		});

		// Wait for nuxt to boot up
		await nuxt.ready();

		// Listen the Nuxt server
		const server = http.createServer(nuxt.render);

		server.listen(() => {
			const httpPort = (server.address() as AddressInfo).port;
			const address = `http://localhost:${httpPort}/home`;

			console.log('Serving app on ' + address);

			resolve(address);
		});
	});
}

// Open the client window
function createWindow(url: string) {
    const window = new BrowserWindow({
		show: false,
        width: 1024,
        height: 768,
        minWidth: 640,
        minHeight: 480,
        icon: path.join(__dirname, '/images/mod_manager_v1.1.ico'),
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
        }
    });

	window.loadURL(url).then(() => window.show());
}