import React from 'react';
import logo from './logo.svg';
import './styles/globals.scss';
import { Link, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import About from './About';
import ModManager from './pages/ModManager';

function App() {
    return (
        <div className="App">
            {/* <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                Edit <code>src/App.tsx</code> and save to reload.
                </p>
                <a
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
                >
                Learn React
                </a>
                
            </header> */}
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="mod-manager" element={<ModManager />} />
            </Routes>
        </div>
    );
}

export default App;
