import { useEffect } from "react";
import Checkbox from "./Checkbox";

interface Iprops {
    items: string[];
}

function toggleSelect(event: Event) {
    let listItem = event.target as HTMLLIElement;
    
    listItem.classList.toggle('selectlist--item__selected');
}

export default function SelectList(props: Iprops) {

    useEffect(() => {
        let listItems = document.querySelectorAll('li.selectlist--item');

        listItems.forEach(element => {
            element.addEventListener('click', toggleSelect);
        });

        return () => {

        }
    }, []);

    return (
        <ul className="selectlist">
            {renderItems(props.items)}
        </ul>
    )
}

function renderItems(items: string[]) {
    return items.map(item => {
        return (
            <li key={item} className="selectlist--item">{item}</li>
        )
    });
}